//console.log("Hello World");

//console.log("");

//let num;
//num = 777 + 888888;

//const myNumber = 25,
//  myString = "Some string";

//const myBool = true;
//const myNull = null;
//const myUndef = undefined;

//console.log(myNumber);
//console.log(myString);
//console.log(myBool);
//console.log(myNull);
//console.log(myUndef);

//console.log("");

//console.log(typeof myNumber);
//console.log(typeof myString);
//console.log(typeof myBool);
//console.log(typeof myNull);
//console.log(typeof myUndef);

//console.log("");

//const obj = {name: "ball"},
//    array = [1,2,3],
//    regexp = /w+/g,
//    func = function(){};

//console.log(typeof obj);
//console.log(typeof array);
//console.log(typeof regexp);
//console.log(typeof func);

//console.log("");

//obj.name = "case";
//array[1] = 777;

//console.log(obj);
//console.log(array);
//console.log(myString.toUpperCase());

//console.log("");

//const N = new Number(7000);
//console.log(typeof N);
//var n = 5000;
//console.log(typeof n);

//var i = 10;
//console.log(++i);
//console.log(i);
//console.log(i++);
//console.log(i);

//var n = 100;
//n +=20;
//console.log(n);
//n *=7;
//console.log(n);
//n /=10;
//console.log(n);

//console.log(5>10);
//console.log(5<10);
//console.log(5>=10);
//console.log(5<=10);
//console.log(10===10);
//console.log(10!==10);
//console.log(10=="10");

//console.log("");

//console.log("Some string");
//console.log('Another string');
//console.log('Some "long" string');
//console.log("Hello world".length);
//console.log('Another \nvery long\nstring');
//console.log('Another \n\tvery long\n\t\tstring');
//console.log('Another \"very long\" string');

//console.log("");

//const string = "Sometimes the same is different";
//console.log(string.length);
//console.log(string.charAt(string.length - 1));
//console.log(string.substring(10));
//console.log(string.substring(10,21));
//console.log(string.slice(-9));
//console.log(string.lastIndexOf("me"));
//console.log(string.split(" "));
//console.log(string.toUpperCase());
//console.log(string.toLowerCase());

const person = {
    constructor: function(name, age, gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        return this;
    },
    greet: function(params) {
        console.log("Hi, my name is " + this.name);
    }
};

let firstPerson, secondPerson, thirdPerson;

firstPerson = Object.create(person).constructor("Valera", 30, "male");
secondPerson = Object.create(person).constructor("Lida", 28, "female");
thirdPerson = Object.create(person).constructor("Gena", 32, "male");

console.log(firstPerson.name);
console.log(secondPerson.age);
console.log(thirdPerson.name);

firstPerson.greet();
secondPerson.greet();
thirdPerson.greet();

const webDeveloper = Object.create(person);
webDeveloper.constructor = function (name, age, gender, skills) {
    person.constructor.apply(this, arguments);
    this.skills = skills || [];
    return this;
};
webDeveloper.develop = function(params){
    console.log("working...");
};

let developer = Object.create(webDeveloper).constructor("Vasya", 22, "male", ["html", "css", "js", "elewenty", "node.js", "nunjucks"]);

console.log(developer.skills);
developer.develop();
console.log(developer.name);
developer.greet();